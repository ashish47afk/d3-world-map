import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import './index.css';
import ReactDOM from 'react-dom';

import React, { Component } from 'react';
import {Chart} from 'primereact/chart';

export class BarChartDemo extends Component {

    render() {
        const data = {
            labels: ['AID', 'APE', 'CCX', 'EAP', 'IAT'],
            datasets: [
                {
                    label: 'Total in CC ',
                    backgroundColor: '#56FFF1',
                    data: [2447, 7546, 1276, 686, 3877]
                },
                {
                    label: 'in EIQ Scope',
                    backgroundColor: '#2F85C3',
                    data: [222, 6268, 1108, 189, 3556]
                },
                {
                  label: 'Rolled Out to',
                  backgroundColor: '#08283E',
                  data: [146, 3462, 495, 189, 3385]
              },
              {
                  label: 'Attended',
                  backgroundColor: '#5C1692',
                  data: [120, 2626, 206, 11, 1373]
              }
            ]
        };

        return (
            <div>
                <Chart type="bar" data={data} />
            </div>
        )
    }
}

export default BarChartDemo;
